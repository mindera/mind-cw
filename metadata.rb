name             'mind-cw'
maintainer       'Mindera'
maintainer_email 'jose.fonseca@mindera.com'
license          'MIT'
description      'Installs/Configures Cloudwatch instance monitoring'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.0.1'

depends 'ark', '~> 0.9.0'
depends 'cron', '= 1.7.6'

supports 'centos', '~> 6.0'
supports 'amazon'
