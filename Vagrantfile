# -*- mode: ruby -*-
# vim: set ft=ruby :

VAGRANTFILE_API_VERSION = '2'
Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
      config.vm.box = 'opscode-centos-6.6'
      config.vm.box_url = 'http://opscode-vm-bento.s3.amazonaws.com/vagrant/virtualbox/opscode_centos-6.6_chef-provisionerless.box'
      config.vm.network "forwarded_port", guest: 8080, host: 8080, auto_correct: true

      config.vm.provider 'virtualbox' do |vb|
        # Use vboxmanage to customize the vm
        vb.customize ['modifyvm', :id, '--memory', '3072']
        vb.customize ['modifyvm', :id, '--cpus', '2']
      end

      if Vagrant.has_plugin?('vagrant-omnibus')
        config.omnibus.chef_version = :latest
      else
        error = "The vagrant-omnibus plugin is not installed! Try running:\nvagrant plugin install vagrant-berkshelf"
        raise Exception, error
      end

      if Vagrant.has_plugin?('vagrant-berkshelf')
        # Use the berkshelf plugin to handle cookbook dependencies
        config.berkshelf.enabled = true
      else
        raise Exception, 'The vagrant-berkshelf plugin is missing - Try: vagrant plugin install vagrant-berkshelf'
      end

      # Provision the box
      config.vm.provision 'chef_solo' do |chef|
        chef.custom_config_path = '.Vagrantfile.chef'
        chef.add_recipe 'mind-cw'
      end
end
