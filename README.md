# Cloudwatch Monitoring

Configures AWS Cloud Watch instance monitoring for disk and memory usage.

## Requirements

Depends on the cookbooks:

 * ark


## Platforms

 * Centos 6+
 * Amazon Linux (WIP)

## Attributes

- node['mind-cw'][:user]   = "cw"
- node['mind-cw'][:group]  = "cw"
- node['mind-cw'][:home_dir] = "/home/#{node['mind-cw'][:user]}"
- node['mind-cw'][:version] = "0.0.1"
- node['mind-cw'][:release_url] = "http://ec2-downloads.s3.amazonaws.com/cloudwatch-samples/CloudWatchMonitoringScripts-v1.1.0.zip"
- node['mind-cw'][:access_key_id] = nil
- node['mind-cw'][:secret_access_key] = nil
- node['mind-cw'][:options] = %w{--disk-space-util  --disk-path=/ --disk-space-used --disk-space-avail --swap-util --swap-used --mem-util --mem-used --mem-avail}
- node['mind-cw'][:cron_minutes] = 2



