#
# Cookbook Name:: mind-cw
# Recipe:: default
#
# Copyright 2015, Mindera
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'cron'

dir="#{node['mind-cw'][:home_dir]}/#{node['mind-cw'][:version]}"

# create group
group node['mind-cw'][:group] do
  action :create
end

# create user
user node['mind-cw'][:user] do
  home node['mind-cw'][:home_dir]
  group node['mind-cw'][:group]
  action :create
end


# Install php native packages
%w{
  perl-Switch
  perl-Sys-Syslog
  perl-LWP-Protocol-https
}.each do |pkg|
  yum_package pkg do
    action :install
    allow_downgrade true
  end
end

# Deploy the AWS Scripts
ark 'cwscripts' do
  url node['mind-cw'][:release_url]
  path dir
  owner node['mind-cw'][:user]
  action :put
  strip_components 0
end


template "#{dir}/credentials.conf" do
    owner node['mind-cw'][:user]
    group node['mind-cw'][:group]
    mode 0644
end

options = ['--from-cron'] + node['mind-cw'][:options] + ["--aws-credential-file #{dir}/credentials.conf"]


cron_d 'cw' do
  minute "*/#{node['mind-cw'][:cron_minutes]}"
  user node['mind-cw'][:user]
  command %Q{#{dir}/cwscripts/aws-scripts-mon/mon-put-instance-data.pl #{(options).join(' ')} || logger -t aws-scripts-mon "status=failed exit_code=$?"}
end
