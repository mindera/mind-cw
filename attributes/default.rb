default_unless['mind-cw'][:user]   = "cw"
default_unless['mind-cw'][:group]  = "cw"
default_unless['mind-cw'][:home_dir] = "/home/#{node['mind-cw'][:user]}"
default_unless['mind-cw'][:version] = "0.0.1"
default_unless['mind-cw'][:release_url] = "http://ec2-downloads.s3.amazonaws.com/cloudwatch-samples/CloudWatchMonitoringScripts-v1.1.0.zip"
default_unless['mind-cw'][:access_key_id] = nil
default_unless['mind-cw'][:secret_access_key] = nil
default_unless['mind-cw'][:options] = %w{--disk-space-util  --disk-path=/ --disk-space-used --disk-space-avail --swap-util --swap-used --mem-util --mem-used --mem-avail}
default_unless['mind-cw'][:cron_minutes] = 2
